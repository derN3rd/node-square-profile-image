const path = require('path')

module.exports = {
  entry: './src/js/main.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'public/js/'),
  },
  module: {
    rules: [
      {
        use: {
          loader: 'babel-loader',
          options: { presets: ['es2015'] },
        },
        test: /\.js$/,
        exclude: /node_modules/,
      },
    ],
  },
}
