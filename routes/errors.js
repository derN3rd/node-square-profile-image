const express = require('express')
const errorhandler = require('errorhandler')

const router = express.Router()
const isProduction = process.env.NODE_ENV === 'production'

router.use((err, req, res, next) => {
  console.error(err)
  if (!isProduction) {
    if (!res.headersSent) {
      return errorhandler()(err, req, res, next)
    }
  } else if (!res.headersSent) {
    return res.status(500).render('error', {
      currentPage: 'error',
      pageTitle: 'Internal Server Error - Crop Pic',
    })
  }
  return undefined
})

router.use((req, res) => {
  if (res.headersSent) {
    return undefined
  }
  return res.status(404).render('404', {
    currentPage: '404',
    pageTitle: '404 - Crop Pic',
  })
})

module.exports = router
