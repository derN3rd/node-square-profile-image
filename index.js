const express = require('express')
const compress = require('compression')
const bodyParser = require('body-parser')
const exphbs = require('express-handlebars')
const { version } = require('./package.json')
const morgan = require('morgan')
const handlebarsHelpers = require('handlebars-helpers')(['comparison'])

const app = express()
const isProduction = process.env.NODE_ENV === 'production'
const hbs = exphbs.create({
  extname: '.hbs',
  defaultLayout: 'main',
  helpers: handlebarsHelpers,
})
const http = new (require('http')).Server(app)

app.disable('x-powered-by')
app.engine('.hbs', hbs.engine)
app.set('view engine', '.hbs')
app.use(compress())
if (isProduction) {
  app.enable('view cache')
}
app.use(express.static('public'))
app.use(bodyParser.json({ limit: '4mb' }))
app.use(bodyParser.urlencoded({ extended: true, limit: '4mb' }))

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}

app.use('/', require('./routes/defaults'))
app.use('/image', require('./routes/image'))

app.use(require('./routes/errors'))

let port = 80
if (process.env.NODE_ENV === 'development') {
  port = 3000
}
if (typeof process.env.PORT !== 'undefined') {
  port = process.env.PORT
}

http.listen(port, () => {
  const mes = `Pic cropper ${(process.env.NODE_ENV === 'production'
    ? 'v'
    : 'dev-') + version} is online on port ${port} c:`
  console.log(mes)
})
