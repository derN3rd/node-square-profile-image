const express = require('express')
const multer = require('multer')
const sharp = require('sharp')
const os = require('os')
const fs = require('fs')

const router = new express.Router()
const upload = multer({ dest: os.tmpdir(), limits: { fileSize: 20000000 } })

const invalidRequest = res => res.status(400).json({ error: 'INVALID_REQUEST' })
const internalError = res =>
  res.status(500).json({ error: 'INTERNAL_SERVER_ERROR' })

const processImage = (imagePath, blurSize) =>
  new Promise((resolve, reject) =>
    fs.readFile(imagePath, (err, imageData) => {
      if (err) {
        return reject(err)
      }
      const sharpImageData = sharp(imageData)
      return sharpImageData
        .metadata()
        .then(metaData => {
          const newImageSize = Math.max(metaData.height, metaData.width)
          const isWidthBigger = newImageSize === metaData.width
          let newImageWidth = metaData.width
          let newImageHeight = metaData.height
          if (metaData.width === metaData.height) {
            // do nothing
          } else if (isWidthBigger) {
            newImageHeight = metaData.width
            newImageWidth = Math.round(
              Math.pow(metaData.width, 2) / metaData.height // eslint-disable-line
            )
          } else {
            newImageHeight = Math.round(
              Math.pow(metaData.height, 2) / metaData.width // eslint-disable-line
            )
            newImageWidth = metaData.height
          }

          if (Math.min(metaData.height, metaData.width) < 200) {
            const customError = new Error('Image is too small to process')
            customError.code = 'IMAGE_TOO_SMALL'
            return reject(customError)
          }

          return sharpImageData
            .resize(newImageWidth, newImageHeight)
            .resize(newImageSize, newImageSize)
            .blur(blurSize || 8)
            .overlayWith(imageData)
            .png()
            .toBuffer()
            .then(outputPicture => {
              const outPic = sharp(outputPicture)
              return outPic
                .metadata()
                .then(meta => {
                  if (Math.max(meta.height, meta.width) > 1500) {
                    return outPic.resize(1500, 1500)
                  }
                  return outPic
                })
                .then(outPic => outPic.jpeg({ quality: 95 }).toBuffer())
            })
            .then(outputPicture => resolve(outputPicture))
        })
        .catch(err => reject(err))
    })
  )

router.post('/process/:blurSize?', (req, res, next) => {
  upload.single('picture')(req, res, err => {
    if (err) {
      if (err.code === 'LIMIT_FILE_SIZE') {
        return res.status(500).json({ error: 'LIMIT_FILE_SIZE' })
      }
      internalError(res)
      return next(err)
    }
    let blurSize = 8
    if (req.params.blurSize) {
      blurSize = parseInt(req.params.blurSize, 10)
      if (Number.isNaN(blurSize)) {
        blurSize = 8
      }
      if (blurSize > 100 || blurSize < 5) {
        return res.status(500).json({ error: 'INVALID_BLUR_VALUE' })
      }
    }

    const picture = req.file
    if (picture && picture.fieldname === 'picture') {
      switch (picture.mimetype) {
        case 'image/jpeg':
        case 'image/jpg':
        case 'image/png':
          if (picture.path) {
            return processImage(picture.path, blurSize)
              .then(outputImage => res.end(outputImage.toString('base64')))
              .then(() =>
                fs.unlink(picture.path, err => {
                  if (err) {
                    console.log(err)
                    // Do nothing, should be cleaned soon because of tmp folder
                  }
                  return undefined
                })
              )
              .catch(err => {
                if (err.code && err.code === 'IMAGE_TOO_SMALL') {
                  return res.status(400).json({ error: 'IMAGE_TOO_SMALL' })
                }
                internalError(res)
                return next(err)
              })
          }
          break
        default:
          return invalidRequest(res)
      }
      return invalidRequest(res)
    }
    return invalidRequest(res)
  })
})

module.exports = router
