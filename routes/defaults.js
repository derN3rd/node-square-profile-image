const express = require('express')

const router = new express.Router()

router.get(['/', '/index'], (req, res, _) =>
  res.render('index', {
    currentPage: 'index',
    pageTitle: 'Profile image cropper',
  })
)

router.get(['/help'], (req, res, _) =>
  res.render('help', {
    currentPage: 'help',
    pageTitle: 'Help | Profile image cropper',
  })
)

module.exports = router
