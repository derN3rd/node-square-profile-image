/* eslint-env browser */
import $ from 'jquery'
import { saveAs } from 'file-saver'

const b64toBlob = b64Data => {
  const byteCharacters = atob(b64Data)
  const byteArrays = []

  for (let offset = 0; offset < byteCharacters.length; offset += 512) {
    const slice = byteCharacters.slice(offset, offset + 512)

    const byteNumbers = new Array(slice.length)
    for (let i = 0; i < slice.length; i += 1) {
      byteNumbers[i] = slice.charCodeAt(i)
    }

    const byteArray = new Uint8Array(byteNumbers)

    byteArrays.push(byteArray)
  }

  const blob = new Blob(byteArrays, { type: 'image/jpeg' })
  return blob
}

const generateAndFetchImage = () => {
  let error = false
  const file = $('#pictureInput')[0].files[0]
  const blurSize = $('#blurSize').val()
  if (file.size > 20000000) {
    error = true
    alert('max upload size is 2mb')
  }
  if (['image/jpg', 'image/jpeg', 'image/png'].indexOf(file.type) === -1) {
    error = true
    alert('Image type not supported')
  }
  if (!error) {
    $('#uploadButton').hide()
    $('#settings').hide()
    $('.spinner').show()
    $('#resetAndDownloadButtons').hide()
    $.ajax({
      // Your server script to process the upload
      url: `/image/process/${blurSize}`,
      type: 'POST',

      // Form data
      data: new FormData($('#uploadForm')[0]),

      // Tell jQuery not to process data or worry about content-type
      // You *must* include these options!
      cache: false,
      contentType: false,
      processData: false,
    })
      .done(data => {
        const blob = b64toBlob(data)
        const urlCreator = window.URL || window.webkitURL
        const imageUrl = urlCreator.createObjectURL(blob)

        document.getElementById('resultImage').src = imageUrl
        $('.spinner').hide()
        $('#resetAndDownloadButtons').show()
        $('#settings').show()
      })
      .fail((jqXHR, _textStatus, _errorThrown) => {
        $('form')[0].reset()
        $('.spinner').hide()
        $('#resetAndDownloadButtons').hide()
        $('#uploadButton').show()
        const errorCode = jqXHR.responseJSON ? jqXHR.responseJSON.error : ''
        switch (errorCode) {
          case 'IMAGE_TOO_SMALL':
            alert('image resolution is too small. please choose a bigger one')
            break
          case 'LIMIT_FILE_SIZE':
            alert('filesize is too big. please choose a smaller image.')
            break
          default:
            alert('Unknown Error. Please try later again.')
        }
      })
  }
}

$(() => {
  $('#pictureInput').on('change', () => generateAndFetchImage())
  $('#resetButton').on('click', () => {
    document.getElementById('resultImage').src = $('#resultImage').data(
      'default-src'
    )
    $('#uploadForm')[0].reset()
    $('#resetAndDownloadButtons').hide()
    $('#uploadButton').show()
    $('#settings').hide()
  })
  function downloader() {
    if (this.status === 200) {
      const myBlob = this.response
      saveAs(myBlob, 'cropped.jpg')
    } else {
      alert('Unknown Error. Please try later again.')
      $('#resetButton').click()
    }
  }
  $('#downloadButton').on('click', () => {
    const xhr = new XMLHttpRequest()
    xhr.open('GET', document.getElementById('resultImage').src, true)
    xhr.responseType = 'blob'
    xhr.onload = downloader
    xhr.send()
  })
  $('#resultImage').on('click', () => {
    if (
      document
        .getElementById('resultImage')
        .src.indexOf('/img/default-pic.png') > -1
    ) {
      $('#pictureInput').click()
    } else {
      $('#downloadButton').click()
    }
  })
  $('#blurSize').on('change', () => {
    if (
      document
        .getElementById('resultImage')
        .src.indexOf('/img/default-pic.png') === -1
    ) {
      generateAndFetchImage()
    }
  })
})
